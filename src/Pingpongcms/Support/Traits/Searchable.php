<?php

namespace Pingpongcms\Support\Traits;

trait Searchable
{
    public function getSearchableFields()
    {
        return property_exists($this, 'searchable') ? $this->searchable : $this->fillable;
    }

    public function scopeSearch($query, $q)
    {
        $pattern = "%{$q}%";
        $query->where(function ($query) use ($pattern, $q) {
            foreach ($this->getSearchableFields() as $search) {
                $search = (array) $search;
                $original = array_get($search, 2, false);
                $column = array_get($search, 0);
                $operator = array_get($search, 1, 'like');
                $criteria = $original ? ($column == $this->getKeyName() ? intval($q) : $q) : $pattern;

                $query->orWhere($column, $operator, $criteria);
            }
        });
    }
}
