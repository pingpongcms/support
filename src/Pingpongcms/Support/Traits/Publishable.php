<?php

namespace Pingpongcms\Support\Traits;

trait Publishable
{
    public static function bootPublishable()
    {
        if (!defined('static::DISABLE_GLOBAL_PUBLISHABLE')) {
            $table = (new static())->getTable();
            static::addGlobalScope('published', function ($query) use ($table) {
                $query->whereNotNull($table.'.published_at');
            });
        }
    }

    public function scopeWithDrafted($query)
    {
        $query->withoutGlobalScope('published');
    }

    public function publishByRequest($request)
    {
        if ($request->published) {
            $this->publish();
        } else {
            $this->unpublish();
        }
    }

    public static function getPublishedAtColumn()
    {
        return defined('static::PUBLISHED_AT') ? static::PUBLISHED_AT : 'published_at';
    }

    /**
     * @deprecated by default model with perform global scope 'published'
     */
    public function scopeOnlyPublished($query)
    {
        $query->whereNotNull($this->getTable().'.'.static::getPublishedAtColumn());
    }

    public function scopeOnlyUnpublished($query)
    {
        $query->withDrafted()->whereNull($this->getTable().'.'.static::getPublishedAtColumn());
    }

    public function scopeOnlyDrafted($query)
    {
        $query->withDrafted()->whereNull($this->getTable().'.'.static::getPublishedAtColumn());
    }

    public function publish()
    {
        $this->fireModelEvent('publishing');
        $this->published_at = $this->freshTimestampString();
        $this->save();
        $this->fireModelEvent('published');
    }

    public function unpublish()
    {
        $this->fireModelEvent('unpublishing');
        $this->published_at = null;
        $this->save();
        $this->fireModelEvent('unpublished');
    }

    public function published()
    {
        return !is_null($this->{static::getPublishedAtColumn()});
    }

    public function unpublished()
    {
        return is_null($this->{static::getPublishedAtColumn()});
    }

    public function getPublishedAttribute()
    {
        return $this->published();
    }

    public function getPublishedStatusAttribute()
    {
        return $this->published() ?
            $this->getPublishedLabel()
            :
            $this->getUnpublishedLabel();
    }

    protected function getPublishedLabel()
    {
        return '<span class="label label-success">Published</span>';
    }

    protected function getUnpublishedLabel()
    {
        return '<span class="label label-danger">Unpublished</span>';
    }

    public function publishedLabel()
    {
        return $this->getPublishedLabel();
    }

    public function unpublishedLabel()
    {
        return $this->getUnpublishedLabel();
    }
}
