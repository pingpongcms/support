<?php

namespace Pingpongcms\Support\Traits;

use Symfony\Component\HttpFoundation\File\UploadedFile;

trait Imageable
{
    /**
     * Delete image from current instance.
     *
     * @return bool
     */
    public function deleteImage()
    {
        if (file_exists($path = $this->getImagePath())) {
            @unlink($path);

            return true;
        }

        return false;
    }

    /**
     * Get image path.
     *
     * @return string
     */
    public function getImagePath()
    {
        return public_path(static::getDestinationPath().'/'.$this->{static::getImageColumn()});
    }

    /**
     * Get image url.
     *
     * @return string
     */
    public function getImageUrl()
    {
        $path = $this->getImagePath();

        if (file_exists($path) && is_file($path)) {
            return asset(static::getDestinationPath().'/'.$this->{static::getImageColumn()});
        }

        return $this->getDefaultImageUrl();
    }

    /**
     * Get default image url.
     * 
     * @return string
     */
    public function getDefaultImageUrl()
    {
        return asset(config('image.default', 'http://placehold.it/1024x768'));
    }

    /**
     * Accessor for "image_url".
     *
     * @param string $value
     *
     * @return string
     */
    public function getImageUrlAttribute($value)
    {
        return $this->getImageUrl();
    }

    /**
     * Determine whether the current file has image.
     *
     * @return bool
     */
    public function hasImage()
    {
        return !empty($this->{static::getImageColumn()}) && file_exists($this->getImagePath());
    }

    public static function bootImageable()
    {
        foreach (['creating', 'updating'] as $event) {
            static::{$event}(function ($model) {
                $file = request()->file(static::getFileInputName());

                if ($file instanceof UploadedFile && $file->isValid()) {
                    $model->{static::getImageColumn()} = static::upload($file);
                }
            });
        }

        static::deleting(function ($model) {
            $model->deleteImage();
        });
    }

    public static function getFileInputName()
    {
        return defined('static::IMAGE_INPUT') ? static::IMAGE_INPUT : 'image';
    }

    public static function getUploadedEventName()
    {
        return defined('static::IMAGE_EVENT') ? static::IMAGE_EVENT : 'image.uploaded';
    }

    public static function getImageColumn()
    {
        return defined('static::IMAGE_COLUMN') ? static::IMAGE_COLUMN : 'image';
    }

    public static function getDestinationPath()
    {
        return defined('static::IMAGE_PATH') ? static::IMAGE_PATH : 'images';
    }

    public static function upload(UploadedFile $file)
    {
        if ($file->isValid()) {
            return upload_image($file, static::getDestinationPath(), static::getUploadedEventName());
        }

        return;
    }

    public function image(array $attributes = [])
    {
        return app('html')->image(
            $this->getImageUrl(),
            $this->{static::getImageColumn()},
            $attributes
        );
    }
}
