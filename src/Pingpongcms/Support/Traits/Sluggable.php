<?php

namespace Pingpongcms\Support\Traits;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;

trait Sluggable
{
    /**
     * Boot the sluggable trait for a model.
     */
    public static function bootSluggable()
    {
        static::saving(function (Model $model) {
            if (!$model->slug) {
                $model->{static::getSlugColumn()} = $model->makeSlug($model->{static::getSlugSource()});
            }
        }, 0);
    }

    public static function getSlugSource()
    {
        return defined('static::MAKE_SLUG_FROM') ? static::MAKE_SLUG_FROM : 'title';
    }

    public static function getSlugColumn()
    {
        return defined('static::SAVE_SLUG_TO') ? static::SAVE_SLUG_TO : 'slug';
    }

    /**
     * Make unique slug from the given string title.
     * 
     * @param string $title
     *
     * @return string
     */
    public function makeSlug($title)
    {
        $slug = Str::slug($title);

        $query = $this->getSluggableQuery();

        if ($id = $this->id) {
            $query = $query->where('id', '!=', $id);
        }

        $db = config('database.default', 'mysql');

        switch ($db) {
            case 'pgsql':
                $operator = '~*';
                break;

            default:
                $operator = 'RLIKE';
                break;
        }

        $count = $query->whereRaw("slug {$operator} '^{$slug}(-[0-9]+)?$'")->count();

        return $count ? "{$slug}-{$count}" : $slug;
    }

    /**
     * Get sluggable query logic.
     *
     * Override this method for adding your custom query logic.
     * 
     * @return mixed
     */
    public function getSluggableQuery()
    {
        return $this;
    }
}
